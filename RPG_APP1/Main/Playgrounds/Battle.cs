﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Battle
    {
        static Random random = new Random();
        Inventory inv = new Inventory();
        Player player = new Player();
        TextArrays text = new TextArrays();

        public class Enemy : PlayingCharacter
        {
            public int expForKill;

            public Enemy(string name, int maxHP, int currentHP, int armorRank, int minDamage, int maxDamage, int STR, int END, int ACR, int AGL, int INT, int expForKill)
            {
                
                this.name = name;
                this.maxHP = maxHP;
                this.currentHP = currentHP;
                this.armorRank = armorRank;
                this.minDamage = minDamage;
                this.maxDamage = maxDamage;
                this.STR = STR;
                this.END = END;
                this.ACR = ACR;
                this.AGL = AGL;
                this.INT = INT;
                this.expForKill = expForKill;
            }
            
        }

        public void battleMethod()
        {
            Enemy enemy = new Enemy("Rat", 10, 10, 1, 1, 2, 1, 1, 1, 5, 1, 10);
            bool hitTrue;
            bool doubleHitTrue;
            Console.WriteLine("BATTLE!!!");

            //Chance to hit
            void attackChanceMethod()
            {
                //lover
                if (Player.AGL < enemy.AGL )
                {
                   int temp = random.Next(1, 10);
                   if(temp <= enemy.AGL - Player.AGL && temp <= 5)
                    {
                        hitTrue = false;
                        Console.WriteLine("MISS");
                    }
                   else
                    {
                        Console.WriteLine("HIT");
                        hitTrue = true;
                    }
                }
                //above
                else if (Player.AGL >= enemy.AGL)
                {
                    int temp = random.Next(1, 100);
                    if (temp <= (Player.AGL - enemy.AGL) * 5)
                    {
                        hitTrue = false;
                        doubleHitTrue = true;
                        Console.WriteLine("DOUBLE HIT");
                    }
                    else
                    {
                        doubleHitTrue = false;
                        Console.WriteLine("HIT");
                        hitTrue = true;
                    }
                }
            }

            void test()
            {
                if (hitTrue)
                {
                    enemy.currentHP -= 1;
                    Console.WriteLine("-1");
                }
            }



            while (enemy.currentHP > 0)
            {
                ConsoleKey key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.A:
                        Console.Clear();
                        attackChanceMethod();
                        break;

                    case ConsoleKey.I:
                        Console.Clear();
                        inv.CharInvPrint();
                        break;

                    case ConsoleKey.C:
                        Console.Clear();
                        player.CharInfo();
                        break;

                    case ConsoleKey.H:
                        text.PrintText(text.helpBattle);
                        break;

                    case ConsoleKey.E:
                        enemy.currentHP = 0;
                        break;
                }
            }
        }
    }
}
