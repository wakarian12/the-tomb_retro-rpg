﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Forge
    {
        static Player player = new Player();
        static Random random = new Random();
        static TextArrays text = new TextArrays();


        static List<Item> ForgeGoodsList = new List<Item>();
        static bool add3 = false;
        static bool add6 = false;
        static bool add9 = false;
        static bool add12 = false;
        static bool add16 = false;
        static bool add20 = false;
        static int spaceCount = 0;

        public void ForgeGoods()
        {
            //Adding items depend on level
            if (Player.level >= 3 && !add3)
            {
                ForgeGoodsList.Add(new Item("Traveler sword", "W", 15, 25, 30, false, 3));
                ForgeGoodsList.Add(new Item("Simple Hauberk", "A", 3, 3, 30, false, 3));
                add3 = true;
            }
            else if (Player.level >= 6 && !add6)
            {
                ForgeGoodsList.Add(new Item("Long sword", "W", 30, 45, 60, false, 6));
                ForgeGoodsList.Add(new Item("Hauberk with helmet", "A", 5, 5, 60, false, 6));
                add6 = true;
            }
            else if (Player.level >= 9 && !add9)
            {
                ForgeGoodsList.Add(new Item("War hammer", "W", 50, 65, 90, false, 9));
                ForgeGoodsList.Add(new Item("Knight`s helm and hauberk", "A", 6, 6, 30, false, 9));
                add9 = true;
            }
            else if (Player.level >= 12 && !add12)
            {
                ForgeGoodsList.Add(new Item("Hetman`s mace", "W", 80, 100, 120, false, 12));
                ForgeGoodsList.Add(new Item("Plate armor", "A", 8, 8, 120, false, 12));
                add12 = true;
            }
            else if (Player.level >= 16 && !add16)
            {
                ForgeGoodsList.Add(new Item("Durandal", "W", 130, 160, 160, false, 16));
                ForgeGoodsList.Add(new Item("Winged plate armor", "A", 10, 10, 160, false, 16));
                add16 = true;
            }
            else if (Player.level >= 20 && !add20)
            {
                ForgeGoodsList.Add(new Item("Balmung", "W", 200, 300, 200, false, 20));
                ForgeGoodsList.Add(new Item("Plate armor of holy knight", "A", 15, 15, 300, false, 20));
                add20 = true;
            }


            //See avalible goods
            if (ForgeGoodsList.Count == 0)
            {
                Console.WriteLine("Sorry, blacksmith didn`t make anything yet.");
            }
            else
            {
                int counter = 1;
                foreach (var item in ForgeGoodsList)
                {
                    switch (item.Type)
                    {
                        case "A":
                            Console.Write("{0}. {1}, Armor Rank: {2}, Price {3} ", counter, item.Name, item.MaxValue, item.Price);
                            counter++;
                            if (item.Name == "Knight`s helm and hauberk")
                            {
                                Console.Write(" (Endurance + 1)");
                            }
                            else if (item.Name == "Winged plate armor")
                            {
                                Console.Write("(Strenght + 1)");
                            }
                            else if (item.Name == "Plate armor of holy knight")
                            {
                                Console.Write("(Strenght + 2 and Endurance + 2)");
                            }
                            Console.WriteLine("");
                            break;
                        case "W":
                            Console.Write("{0}. {1}, Damage {2}-{3}, Price {4}", counter, item.Name, item.MinValue, item.MaxValue, item.Price);
                            counter++;
                            Console.WriteLine("");
                            break;
                    }
                    spaceCount++;
                    if (spaceCount >= 2)
                    {
                        Console.WriteLine("");
                        spaceCount = 0;
                    }
                }
            }
            //Buy goods
            int chooseVar;
            int InputIntMethod()
            {
                try
                {
                    return chooseVar = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    return chooseVar = ForgeGoodsList.Count + 1;
                }
            }
            InputIntMethod();
            if (chooseVar <= ForgeGoodsList.Count)
            {
                if (Player.gold >= ForgeGoodsList[chooseVar - 1].Price)
                {
                    Player.gold -= ForgeGoodsList[chooseVar - 1].Price;
                    Inventory.InventoryList.Add(ForgeGoodsList[chooseVar - 1]);
                    Console.WriteLine("You bought {0}. It cost`s {1} pieces of gold.", ForgeGoodsList[chooseVar - 1].Name, ForgeGoodsList[chooseVar - 1].Price);
                }
                else
                {
                    Console.WriteLine("Sorry, but you don`t have enought gold.");
                }
            }
        }
    }
}
