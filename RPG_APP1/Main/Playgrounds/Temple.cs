﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Temple
    {
        static Player player = new Player();
        TextArrays text = new TextArrays();
        

        //PREACH
        public void PreachMethod()
        {
            if (Tomb.currentTombLevel == 1)
            {
                text.PrintText(text.templePreach1);
            }
        }

        //BUY HEALING POTIONS
        public void healingPotionsBuyMethod()
        {
            text.PrintText(text.templeBuyHealingPotionsArr);
            string input = Console.ReadLine();
            switch (input)
            {
                case "YES":
                    if (Player.gold >= 10)
                    {
                        Player.gold -= 10;
                        Player.healPotions++;
                        Console.WriteLine("Priest goes down to basement, and returning with glass bottle of red liquid.");
                    }
                    else
                    {
                        Console.WriteLine("Sorry, you didn`t have enought gold.");
                    }
                    break;

                case "NO":
                    Console.WriteLine("Priest shrug: 'As you wish'.");
                    break;

                default:
                    healingPotionsBuyMethod();
                    break;
            }
        }
    }
}
