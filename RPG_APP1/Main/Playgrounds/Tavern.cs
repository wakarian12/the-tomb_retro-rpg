﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Tavern
    {
        static Player player = new Player();
        TextArrays text = new TextArrays();

        public void TavernSleepMethod()
        {
            Player.currentHP = Player.maxHP;
            text.PrintText(text.tavernSleepTextArr);
        }
        public void TavernEatMethod()
        {
            Player.currentHP += 5;
            if (Player.currentHP > Player.maxHP)
                Player.currentHP = Player.maxHP;
            text.PrintText(text.tavernEatTextArr);
        }

        public void TavernChatMethod()
        {
            text.PrintText(text.tavernChatArr);
        }
    }
}
