﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Tomb
    {
        static Random random = new Random();

        static public int currentTombLevel = 1;
        static public string tombMapPath = @"Tomb" + currentTombLevel;

        public void randomEncounter()
        {
            int randomNumber = random.Next(1, 20);

            if (randomNumber < 5)
            {
                PlaygroundSwitch.currentPlayground = "Battle";
            }
            else if (randomNumber > 15)
            {
                Console.WriteLine("You found Treasure!");
                Player.haveTreasures++;
            }
        }
        
    }
}
