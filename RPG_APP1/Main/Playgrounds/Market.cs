﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Market
    {
        static Player player = new Player();
        static Random random = new Random();
        static TextArrays text = new TextArrays();


        //SELL TREASURES METHOD
        public void SellTreasures()
        {
            double temp;
            temp = Player.haveTreasures * (10 * Player.inteligenceModPrice);
            Player.gold += Convert.ToInt32(temp);
            Player.haveTreasures = 0;
        }

        //FIND BERSERK POTION METHOD
        public void FindSpecPotions()
        {
            int randomNumber;
            if (Player.INT <= 40)
            {
                randomNumber = random.Next(0, 40);
            }
            else
            {
                randomNumber = 1;
            }

            if (randomNumber < Player.INT)
            {
                text.PrintText(text.marketBuyBerserkPotionsArr);
                string input = Console.ReadLine();
                void berserkPotionsBuyInputMethod()
                {
                    switch (input)
                    {
                        case "YES":
                            if (Player.gold >= 20)
                            {
                                Player.gold -= 20;
                                Player.berserkPotions++;
                                Console.WriteLine("Gipsy merchant takes your gold and dissapire into shadows, leaving you with powerful potion.");
                            }
                            else
                            {
                                Console.WriteLine("Sorry, you didn`t have enought gold.");
                            }
                            break;

                        case "NO":
                            Console.WriteLine("You go away, not willing to drink heresy potions.");
                            break;

                        default:
                            berserkPotionsBuyInputMethod();
                            break;
                    }
                }
                berserkPotionsBuyInputMethod();
            }
            else
            {
                Console.WriteLine("You didn`t find anything worth interest.");
            }
        }

        //MARKET GOODS METHOD
        public void SeeMarketGoods()
        {
            List<Item> MarketGoodsList = new List<Item>();
            //Adding items
            //Level 1
            MarketGoodsList.Add(new Item("Hard Wooden stick", "W", 3, 5, 10, false, 1));
            MarketGoodsList.Add(new Item("Linen shirt", "A", 1, 1, 10, false, 1));
            //Level 2
            MarketGoodsList.Add(new Item("Kitchen knife", "W", 6, 12, 20, false, 2));
            MarketGoodsList.Add(new Item("Beautiful linen shirt", "A", 1, 1, 20, false, 2));
            //Level 3
            MarketGoodsList.Add(new Item("Hunter`s knife", "W", 10, 15, 30, false, 3));
            MarketGoodsList.Add(new Item("Hunter`s outfit", "A", 2, 2, 30, false, 3));
            //Level 4
            MarketGoodsList.Add(new Item("Northen long knife", "W", 15, 25, 40, false, 4));
            MarketGoodsList.Add(new Item("Down jacket", "A", 3, 3, 40, false, 4));
            //Level 6
            MarketGoodsList.Add(new Item("Mace", "W", 25, 35, 60, false, 6));
            MarketGoodsList.Add(new Item("Rusty hauberk", "A", 4, 4, 60, false, 6));
            //Level 8
            MarketGoodsList.Add(new Item("Eastern saber", "W", 35, 45, 80, false, 8));
            MarketGoodsList.Add(new Item("Plate helm with hauberk", "A", 6, 6, 80, false, 8));
            //Level 10
            MarketGoodsList.Add(new Item("Repica of Balmung", "W", 55, 70, 100, false, 10));
            MarketGoodsList.Add(new Item("Tempered in blood hauberk", "A", 7, 7, 100, false, 10));

            foreach (var item in MarketGoodsList)
            {
                double temp;
                temp = Convert.ToDouble(item.Price) / Player.inteligenceModPrice;
                item.Price = Convert.ToInt32(temp);

            }

            //See items
            int counter = 1;
            foreach (var item in MarketGoodsList)
            {
                switch (item.Type)
                {
                    case "A":
                        Console.Write("{0}. {1}, Armor Rank: {2}, Price {3} ", counter, item.Name, item.MaxValue, item.Price);
                        counter++;
                        if (item.Name == "Beautiful linen shirt")
                        {
                            Console.Write(" (Prices are better)");
                        }
                        else if (item.Name == "Tempered in blood hauberk")
                        {
                            Console.Write("(The strength will flowing in your blood, but you going to lost some of your health)");
                        }
                        Console.WriteLine("");
                        break;
                    case "W":
                        Console.Write("{0}. {1}, Damage {2}-{3}, Price {4}", counter, item.Name, item.MinValue, item.MaxValue, item.Price);
                        counter++;
                        Console.WriteLine("");
                        break;
                }
            }

            //Buy items
            int chooseVar;
            int InputIntMethod()
            {
                try
                {
                    return chooseVar = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    return chooseVar = MarketGoodsList.Count + 1;
                }
            }
            InputIntMethod();
            if (chooseVar <= MarketGoodsList.Count)
            {
                if (Player.gold >= MarketGoodsList[chooseVar - 1].Price)
                {
                    Player.gold -= MarketGoodsList[chooseVar - 1].Price;
                    Inventory.InventoryList.Add(MarketGoodsList[chooseVar - 1]);
                    Console.WriteLine("You bought {0}. It cost`s {1} pieces of gold.", MarketGoodsList[chooseVar - 1].Name, MarketGoodsList[chooseVar - 1].Price);
                }
                else
                {
                    Console.WriteLine("Sorry, but you don`t have enought gold.");
                }

            }
        }
    }
}
