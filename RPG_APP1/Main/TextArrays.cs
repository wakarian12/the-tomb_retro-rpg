﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{

    class TextArrays
    {
        static Random random = new Random();

        public string[] charCreationTextArr0 = { "Your sex is:", "1.Male", "2.Female", "" };
        public string[] charCreationTextArr1 = { "Your Childhood story:", "1. Farmer", "2. Blacksmith", "3. Hunter", "4. Scribble", "5. Novice", " " };
        public string[] charCreationFarmerStory = { "", "All your childhood you spend on feudal field, working until complete impoverishment. ", "As the result, your endurance is improved.", "" };
        public string[] charCreationBlacksmithStory = { "", "Fire of father`s forge tempered you, and your muscles is as strong as a metal.", "Your strength has increased.", "" };
        public string[] charCreationHunterStory = { "", "Father always told you – one accurate shoot could save whole family from death.", "So you train your accuracy very well.", "" };
        public string[] charCreationScribblerStory = { "", "Street and community of other cruel children raise you better than any parent. ", "Those, who don`t have high agility haven`t lived for a long there. ", "You are still alive.", "" };
        public string[] charCreationNoviceStory = { "", "Your childhood was spend in monastery, as church servant.", "In addition to receipts of wine and beer, you have brought out some other knowledge. ", "Your intelligence is much higher than in ordinary peasant.", "" };
        public string[] charCreationTextArr2 = { "Which attribure you want to increase?", "1. Strength", "2. Accuracy", "3. Intelligence", "4. Agility", "5. Endurance", " " };
        public string[] charCreationTextColor = { "Choose your color:", "1.White", "2.Yellow", "3.Black", "4.Green", "5.Purple" };
        public string[] charCreationTextArr3 = { "Congratulations!You`ve created your character, and ready for adventure!", "Let`s look at you:", " ", "MAIN" };
        public string[] mainMenuText = { "WELCOME!", "", "Create new character (START)", "Help", "Exit" };
        public string[] mainMenuHelp = { " ", "For playing, input your actions like this:", "SAMPLE ACTION", "", "If you want to see all avalible actions, type:", "HELP" };
        public string[] tavernHelp = { " ", "You are in Tavern now.", "Here you can:", "SLEEP - order a room to restore all HP", "EAT - order some food and drinks to restore minor HP", "CHAT - talk to common folks and innkeeper"};
        public string[] tavernSleepTextArr = { "", "You sleep well, and restore all your health.", "" };
        public string[] tavernEatTextArr = { "", "Food and drinks are very tasty, and restore some of your health.", "" };
        public string[] tavernChatArr = { "", "Priest: Life is too short, my friend. Dont waste it.", "" };
        public string[] tavernExitTextArr = { "", "You push heavy tavern door, and walk outside.", "Fresh air fills your lungs, but after a second, the smells of the city begin to be heard.", "You are in the city street now.", "" };
        public string[] townHelp = { "", "You are in Town now", "Here you can:", "TAVERN - go into Tavern", "MARKET - enter the Market Square", "FORGE - visit Forge", "TEMPLE - walk into Temple", "TOMB - go down into the tomb", "CHAT - talk to people, wandering the streets", "" };
        public string[] townTavernEnterTextArr = { "", "You walk into Tavern, and smell of food greet`s you.", "Innkeeper smiles to you, and ask what he can do", "" };
        public string[] townForgeEnterTextArr = { "", "You walk into Forge ", "" };
        public string[] townMarketEnterTextArr = { "", "You walk into Market Square", "" };
        public string[] townTempleEnterTextArr = { "", "You walk into Temple", "" };
        public string[] townTombEnterTextArr = { "", "You walk into Tomb", "" };
        public string[] marketHelp = { "", "You are on Market Square now", "Here you can:", "SELL - sell treasures, founded into the Tomb", "GOODS - see avialable weapon and armor", "SPEC - look for some specific supples", "CHAT - talk to merchants", "OUT - get back into town", "" };
        public string[] marketExitTextArr = { "", "You return to the city, eliminating the insolent merchants.", "" };
        public string[] marketBuyBerserkPotionsArr = { "", "After long seeking into the darkest corners of market, you found it!", "ATTRIBUTE POTION!", "It costs 20 gold. Will you buy it? (YES/NO)", "" };
        public string[] marketChatArr = { "", "Merchant Burns: 'Yeeees?..'", "" };
        public string[] forgeHelp = { "", "You are into the Forge now", "Here you can:", "GOODS - see weapon and armor, crafted by blacksmith", "CHAT - talk to blacksmith", "OUT - get back into town", "" };
        public string[] forgeChatArr = { "", "Blacksmith: Welcome to my forge!", ""};
        public string[] templePreach1 = { "", "It`s first preach", "" };
        public string[] templeBuyHealingPotionsArr = { "", "Welcome, child!", "Do you want to bye some potions?", "10 pieces of gold for each!", ""};
        public string[] templeChatArr = {"", "Priest: 'Do you want to chat, my child?'", "" };
        public string[] templeHelp = { " ", "You are in Temple now.", "Here you can:", "POTION – buy healing potion.", "PREACH – ask priest for preach.", "CHAT – talk to priest, church servants and parishioners." };
        public string[] testArr = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
        public string[] defaultHelpArr = {"", "Default Actions:", "WASD - Moving", "H - Help", "I - Inventory", "C - Character Sheet", };
        public string[] helpBattle = {"A - Attack", "H - Help", "I - Inventory", "C - Character Sheet", "E - exit" };

        public void PrintText(string[] printArr)
        {
            string[] localPrintArr;
            localPrintArr = printArr;
            foreach (string element in localPrintArr)
                Console.WriteLine(element);
        }

        public void ChatMethod(string[] currentChatArr)
        {
            string[] localArr;
            localArr = currentChatArr;
            int randomReplicIndex = random.Next(0, localArr.Length);
            Console.WriteLine(localArr[randomReplicIndex]);
        }
    }
    
}
