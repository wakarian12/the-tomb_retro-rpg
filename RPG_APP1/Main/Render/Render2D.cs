﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Render2D
    {

        Inventory inv = new Inventory();
        Player player = new Player();
        TextArrays text = new TextArrays();
        char playerMark = '☻';
        public int mapHeight;
        public int mapWidth;
        static public char[,] mapChar;
        static public string path;

        //Loading Map file
        public void MapLoad(string path)
        {
            Render2D.path = path;
            //"g:\Programming\C# Learning\MovementTest\Map1.txt"
            string[] mapString = File.ReadAllLines(path);
            mapHeight = mapString.Length;
            mapWidth = mapString[0].Length;

            mapChar = new char[mapWidth, mapHeight];

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    mapChar[x, y] = mapString[y][x];
                }
            }
        }
        
        //Print Field
        private void PrintField()
        {
            for (int k = 0; k < mapHeight; k++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    if (mapChar[j, k] == '.' || mapChar[j, k] == 'o') Console.ForegroundColor = ConsoleColor.White;
                    else if (mapChar[j, k] == ',') Console.ForegroundColor = ConsoleColor.Green;
                    else if (mapChar[j, k] == '#') Console.ForegroundColor = ConsoleColor.DarkGray;
                    else if (mapChar[j, k] == '☺') Console.ForegroundColor = ConsoleColor.DarkYellow;
                    else if (mapChar[j, k] == '^')
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                    }
                    else if (mapChar[j, k] == '☻')
                    {
                        switch (Player.color)
                        {
                            case "White":
                                Console.ForegroundColor = ConsoleColor.White;
                                break;

                            case "Yellow":
                                Console.ForegroundColor = ConsoleColor.DarkYellow;
                                break;

                            case "DarkGray":
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                break;

                            case "DarkGreen":
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                break;

                            case "Magenta":
                                Console.ForegroundColor = ConsoleColor.Magenta;
                                break;
                        }
                    }
                    else if (mapChar[j, k] == '═' || mapChar[j, k] == '╗' || mapChar[j, k] == '╔' || mapChar[j, k] == '║' || mapChar[j, k] == '│' || mapChar[j, k] == '╕' || mapChar[j, k] == '╒') Console.ForegroundColor = ConsoleColor.DarkRed;
                    else Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(mapChar[j, k]);
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" ");
            }
            Console.WriteLine("You are at X:{0} Y:{1};", Player.playerX, Player.playerY);
        }

        public void StartRender()
        {
            MapLoad(Render2D.path);
            mapChar[Player.playerX, Player.playerY] = playerMark;

            Console.Clear();
            PrintField();
        }

        //Meet Wall method
        void MeetWall()
        {
            MapLoad(Render2D.path);
            mapChar[Player.playerX, Player.playerY] = playerMark;
            Console.Clear();
            PrintField();
        }

        //------------Move Method-----------------//
        public void Render2DMove()
        {
            ConsoleKey key = Console.ReadKey(true).Key;
            //Pressing keys method
            void MoveOnKey()
            {
                //Pressind D
                switch (key)
                {
                    case ConsoleKey.D:
                        MapLoad(Render2D.path);
                        Player.playerX++;
                        mapChar[Player.playerX, Player.playerY] = playerMark;
                        Console.Clear();
                        PrintField();
                        break;

                    case ConsoleKey.A:
                        MapLoad(Render2D.path);
                        Player.playerX--;
                        mapChar[Player.playerX, Player.playerY] = playerMark;
                        Console.Clear();
                        PrintField();
                        break;

                    case ConsoleKey.W:
                        MapLoad(Render2D.path);
                        Player.playerY--;
                        mapChar[Player.playerX, Player.playerY] = playerMark;
                        Console.Clear();
                        PrintField();
                        break;

                    case ConsoleKey.S:
                        MapLoad(Render2D.path);
                        Player.playerY++;
                        mapChar[Player.playerX, Player.playerY] = playerMark;
                        Console.Clear();
                        PrintField();
                        break;

                    case ConsoleKey.I:
                        Console.Clear();
                        inv.CharInvPrint();
                        break;

                    case ConsoleKey.C:
                        Console.Clear();
                        player.CharInfo();
                        break;

                    case ConsoleKey.H:
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.White;
                        switch (PlaygroundSwitch.currentPlayground)
                        {
                            case "Tavern":
                                text.PrintText(text.tavernHelp);
                                text.PrintText(text.defaultHelpArr);
                                break;
                            case "Town":
                                text.PrintText(text.townHelp);
                                text.PrintText(text.defaultHelpArr);
                                break;
                            case "Market":
                                text.PrintText(text.marketHelp);
                                text.PrintText(text.defaultHelpArr);
                                break;
                            case "Forge":
                                text.PrintText(text.forgeHelp);
                                text.PrintText(text.defaultHelpArr);
                                break;
                            case "Temple":
                                text.PrintText(text.templeHelp);
                                text.PrintText(text.defaultHelpArr);
                                break;
                        }
                        break;
                }
            }
            //Movement
            if (mapChar[Player.playerX, Player.playerY + 1] == '#' || mapChar[Player.playerX, Player.playerY - 1] == '#' || mapChar[Player.playerX - 1, Player.playerY] == '#' || mapChar[Player.playerX + 1, Player.playerY] == '#')
            {
                if (mapChar[Player.playerX, Player.playerY + 1] == '#' && key == ConsoleKey.S) MeetWall();
                else if (mapChar[Player.playerX, Player.playerY - 1] == '#' && key == ConsoleKey.W) MeetWall();
                else if (mapChar[Player.playerX + 1, Player.playerY] == '#' && key == ConsoleKey.D) MeetWall();
                else if (mapChar[Player.playerX - 1, Player.playerY] == '#' && key == ConsoleKey.A) MeetWall();
                else MoveOnKey();
            }
            else
            {
                MoveOnKey();
            }
        }
    }
}
