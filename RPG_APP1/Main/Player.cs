﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    public class Player : PlayingCharacter
    {
        //----------------------LOAD VARIABLES--------------//
        static string action = "GAME STARTED";
        //Create character static object
         TextArrays text = new TextArrays();
         Inventory inv = new Inventory();
        //-----------------------------------------------//

        static public string sex;
        static public string color;
        static public string childhood;
        static public int gold = 0;
        static public double haveTreasures = 1;
        static public int healPotions = 0;
        static public int berserkPotions = 0;
        static public int level = 1;
        static public double dammageMod;
        static public int dodgeMod;
        static public double inteligenceMod;
        static public double inteligenceModPrice;
        static public string name;
        static public int maxHP;
        static public int currentHP;
        static public int armorRank;
        static public int minDamage;
        static public int maxDamage;
        static public int STR = 1;
        static public int END = 1;
        static public int ACR = 1;
        static public int AGL = 1;
        static public int INT = 1;
        static public int playerX;
        static public int playerY;

        //CHARACTER INFO METHOD
        public void CharInfo()
        {
            Console.WriteLine(" ");
            Console.WriteLine("Now you are in " + PlaygroundSwitch.currentPlayground);
            Console.WriteLine(" ");
            Console.WriteLine("Name: " + Player.name);
            Console.WriteLine("Sex: " + Player.sex);
            Console.WriteLine("Childhood: " + Player.childhood);
            Console.WriteLine("Level: " + Player.level);
            Console.WriteLine("Gold: " + Player.gold);
            Console.WriteLine("Armor Rank: " + Player.armorRank);
            Console.WriteLine("Damage: {0}-{1} ", Player.minDamage, Player.maxDamage);
            Console.WriteLine("ATTRIBUTES");
            Console.WriteLine("HP: {0} / {1} ", Player.currentHP, Player.maxHP);
            Console.WriteLine("Strength: " + Player.STR);
            Console.WriteLine("Accuracy: " + Player.ACR);
            Console.WriteLine("Intelligence: " + Player.INT);
            Console.WriteLine("Agility: " + Player.AGL);
            Console.WriteLine("Endurance: " + Player.END);
            Console.WriteLine(" ");
        }

        //CHARACTER CALCULATE STATS METHOD
        public void CalculatePlayerStats()
        {
            Player.maxHP = Player.END * 10;
            //Damage modificator
            if (Player.STR == 1)
                Player.dammageMod = 0.5;
            else if (Player.STR == 2)
            {
                Player.dammageMod = 1;
            }
            else
            {
                Player.dammageMod = Player.STR * 1.5;
            }
            //Dodge modificator
            Player.dodgeMod = Player.AGL * 2;
            if (Player.dodgeMod > 90)
            {
                Player.dodgeMod = 90;
            }
            //Inteligence modificator
            Player.inteligenceMod = Player.INT / 2;
            //price Modificator
            Player.inteligenceModPrice = 1 + Player.INT * 0.01 * 5;
            //Damage
            foreach (var item in Inventory.InventoryList)
            {
                if (item.Type == "W" && item.IsEquipped)
                {
                    Player.minDamage = item.MinValue;
                    Player.maxDamage = item.MaxValue;
                }
            }
            //Armor Rank
            foreach (var item in Inventory.InventoryList)
            {
                if (item.Type == "A" && item.IsEquipped)
                {
                    Player.armorRank = item.MaxValue;
                }
            }
        }

        //--------------CHARACTER CREATION---------------//
        public void CharCreaion()
        {
            //Input Method
            int chooseVar;
            int InputIntMethod()
            {
                try
                {
                    return chooseVar = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    return chooseVar = 0;
                }
            }
            //Name Creation
            Console.WriteLine("");
            Console.Write("Youre name is: ");
            Player.name = Console.ReadLine();
            Console.WriteLine(" ");

            //Sex creation
            void SexCreation()
            {
                text.PrintText(text.charCreationTextArr0);
                InputIntMethod();
                while (chooseVar == 0)
                {
                    Console.WriteLine("Please, enter 1 or 2:");
                    Console.WriteLine(" ");
                    InputIntMethod();
                }
                switch (chooseVar)
                {
                    case 1:
                        Player.sex = "Male";
                        break;

                    case 2:
                        Player.sex = "Female";
                        break;

                    default:
                        Console.WriteLine("Please, enter 1 or 2:");
                        Console.WriteLine(" ");
                        SexCreation();
                        break;

                }
            }
            SexCreation();
            Console.WriteLine(" ");

            //Childhood creation
            void ChildhoodCreationMethod()
            {
                text.PrintText(text.charCreationTextArr1);
                InputIntMethod();
                while (chooseVar == 0)
                {
                    Console.WriteLine("Please, enter 1, 2, 3, 4 or 5:");
                    Console.WriteLine(" ");
                    InputIntMethod();
                }
                Console.WriteLine(" ");
                switch (chooseVar)
                {
                    case 1:
                        Player.childhood = "Farmer";
                        text.PrintText(text.charCreationFarmerStory);
                        Player.END += 2;
                        Inventory.InventoryList.Add(new Item("Scythe", "W", 1, 5, 10, true, 1));
                        Inventory.InventoryList.Add(new Item("Father`s Linen shirt", "A", 0, 1, 5, true, 1));
                        break;
                    case 2:
                        Player.childhood = "Blacksmith";
                        text.PrintText(text.charCreationBlacksmithStory);
                        Player.STR += 2;
                        Inventory.InventoryList.Add(new Item("Father`s Forge Hammer", "W", 2, 6, 10, true, 1));
                        Inventory.InventoryList.Add(new Item("Blacksmith apron", "A", 1, 1, 5, true, 1));
                        break;
                    case 3:
                        Player.childhood = "Hunter";
                        text.PrintText(text.charCreationHunterStory);
                        Player.ACR += 2;
                        Inventory.InventoryList.Add(new Item("Old bow", "W", 1, 3, 10, true, 1));
                        Inventory.InventoryList.Add(new Item("Old Hunter`s outfit", "A", 2, 1, 5, true, 1));
                        break;
                    case 4:
                        Player.childhood = "Scribble";
                        text.PrintText(text.charCreationScribblerStory);
                        Player.AGL += 2;
                        Inventory.InventoryList.Add(new Item("Knuckles", "W", 1, 3, 10, true, 1));
                        Inventory.InventoryList.Add(new Item("Cranking rags", "A", 0, 1, 5, true, 1));
                        Player.gold += 10;
                        break;
                    case 5:
                        Player.childhood = "Novice";
                        text.PrintText(text.charCreationNoviceStory);
                        Player.INT += 2;
                        Inventory.InventoryList.Add(new Item("Piligrim staff", "W", 1, 5, 10, true, 1));
                        Inventory.InventoryList.Add(new Item("Piligrim cassock", "A", 1, 1, 5, true, 1));
                        break;
                    default:
                        Console.WriteLine("Please, enter 1, 2, 3, 4 or 5.");
                        Console.WriteLine(" ");
                        ChildhoodCreationMethod();
                        break;
                }
            }
            ChildhoodCreationMethod();

            //Atributes
            void AttributeCreationMethod()
            {
                text.PrintText(text.charCreationTextArr2);
                InputIntMethod();
                while (chooseVar == 0)
                {
                    Console.WriteLine("Please, enter 1, 2 or 3:");
                    Console.WriteLine(" ");
                    InputIntMethod();
                }
                Console.WriteLine(" ");
                switch (chooseVar)
                {
                    case 1:
                        Player.STR++;
                        break;
                    case 2:
                        Player.ACR++;
                        break;
                    case 3:
                        Player.INT++;
                        break;
                    case 4:
                        Player.AGL++;
                        break;
                    case 5:
                        Player.END++;
                        break;
                    default:
                        Console.WriteLine("Please, enter 1, 2, 3, 4 or 5.");
                        Console.WriteLine(" ");
                        AttributeCreationMethod();
                        break;
                }
            }
            AttributeCreationMethod();
            AttributeCreationMethod();

            //Color choose
            void ColorCreation()
            {
                text.PrintText(text.charCreationTextColor);
                InputIntMethod();
                while (chooseVar == 0)
                {
                    Console.WriteLine("Please, enter 1 or 2:");
                    Console.WriteLine(" ");
                    InputIntMethod();
                }
                switch (chooseVar)
                {
                    case 1:
                        Player.color = "White";
                        break;

                    case 2:
                        Player.color = "Yellow";
                        break;

                    case 3:
                        Player.color = "DarkGray";
                        break;

                    case 4:
                        Player.color = "DarkGreen";
                        break;

                    case 5:
                        Player.color = "Magenta";
                        break;

                    default:
                        Console.WriteLine("Please, enter 1, 2, 3, 4 or 5.");
                        Console.WriteLine(" ");
                        ColorCreation();
                        break;

                }
            }
            ColorCreation();
            Console.WriteLine(" ");

            //Calculate Player Stats
            CalculatePlayerStats();
            Player.currentHP = Player.maxHP;

            //Final
            text.PrintText(text.charCreationTextArr3);
            Player.playerX = 10;
            Player.playerY = 6;
            PlaygroundSwitch.currentPlayground = "Tavern";
            CharInfo();
            Console.WriteLine("You are stay in 'Drunk Bishop Inn' now. (Press H for Help)");
        }
    }


}
