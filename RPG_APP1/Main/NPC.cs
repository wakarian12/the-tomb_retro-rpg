﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    public class PlayingCharacter
    {
        public string name;
        public int maxHP;
        public int currentHP;
        public int armorRank;
        public int minDamage;
        public int maxDamage;
        public int STR = 1;
        public int END = 1;
        public int ACR = 1;
        public int AGL = 1;
        public int INT = 1;
    }
}
