﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    public class Item
    {
        private string name;
        private string type;
        private int minValue;
        private int maxValue;
        private int price;
        private bool isEquipped;
        private int levelSTR;

        public Item(string name, string type, int minValue, int maxValue, int price, bool isEquipped, int levelSTR)
        {
            this.name = name;
            this.type = type;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.price = price;
            this.isEquipped = isEquipped;
            this.levelSTR = levelSTR;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public int MinValue
        {
            get { return minValue; }
            set { minValue = value; }
        }
        public int MaxValue
        {
            get { return maxValue; }
            set { maxValue = value; }
        }
        public int Price
        {
            get { return price; }
            set { price = value; }
        }
        public bool IsEquipped
        {
            get { return isEquipped; }
            set { isEquipped = value; }
        }
        public int LevelSTR
        {
            get { return levelSTR; }
            set { levelSTR = value; }
        }
    }
}
