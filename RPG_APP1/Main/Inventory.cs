﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_APP1.Main
{
    class Inventory
    {
        static TextArrays text = new TextArrays();
        static Player player = new Player();
        static public List<Item> InventoryList = new List<Item>();

        //Print inventory
        public void CharInvPrint()
        {
            Console.WriteLine(" ");
            Console.WriteLine("Your inventory:");
            Console.WriteLine(" ");
            Console.WriteLine("Healing potions: {0}", Player.healPotions);
            Console.WriteLine("Berserk potions: {0}", Player.berserkPotions);
            Console.WriteLine("Treasures: {0}", Player.haveTreasures);
            Console.WriteLine("Gold: {0}", Player.gold);
            Console.WriteLine(" ");
            int counter = 1;
            foreach (var item in InventoryList)
            {
                switch (item.Type)
                {
                    case "A":
                        Console.Write("{0}. {1}, Armor Rank: {2}, Price {3} ", counter, item.Name, item.MaxValue, item.Price);
                        counter++;
                        if (item.IsEquipped)
                        {
                            Console.WriteLine(" (E)");
                        }
                        else
                        {
                            Console.WriteLine(" ");
                        }
                        break;
                    case "W":
                        Console.Write("{0}. {1}, Damage {2}-{3}, Price {4}", counter, item.Name, item.MinValue, item.MaxValue, item.Price);
                        counter++;
                        if (item.IsEquipped)
                        {
                            Console.WriteLine(" (E)");
                        }
                        else
                        {
                            Console.WriteLine(" ");
                        }
                        break;
                }
            }
            //Choose from inventory
            int chooseVar;
            int InputIntMethod()
            {
                try
                {
                    return chooseVar = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    return chooseVar = InventoryList.Count + 1;
                }
            }
            InputIntMethod();
            if (chooseVar <= InventoryList.Count)
            {
                string selectedItemType = InventoryList[chooseVar - 1].Type;
                foreach (var item in InventoryList)
                {
                    if (item.Type == selectedItemType)
                    {
                        item.IsEquipped = false;
                        InventoryList[chooseVar - 1].IsEquipped = true;
                    }
                }
                player.CalculatePlayerStats();
                CharInvPrint();
            }
        }
    }
}
