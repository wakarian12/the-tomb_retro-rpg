﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPG_APP1.Main
{
    class Program
    { 

//----------------------LOAD VARIABLES--------------//
        static string action = "GAME STARTED";
        //Create character static object
        static Player player = new Player();
        static TextArrays text = new TextArrays();
        static Inventory inv = new Inventory();
        static Tavern tavern = new Tavern();
        static Market market = new Market();
        static Forge forge = new Forge();
        static Temple temple = new Temple();
        static Tomb tomb = new Tomb();
        static Battle battle = new Battle();
        static Render2D render2d = new Render2D();

//---------------------------------------MAIN--------------------------------//
        static void Main(string[] args)
        {
            //---Create Test Character----//

           /* Player.name = "Ailand";
            Player.color = "White";
            Player.sex = "Male";
            Player.childhood = "Farmer";
            Player.END += 2;
            Inventory.InventoryList.Add(new Item("Scythe", "W", 1, 5, 10, true, 1));
            Inventory.InventoryList.Add(new Item("Father`s Linen shirt", "A", 0, 1, 5, true, 1));
            Player.level = 20;
            Player.AGL += 1;
            Player.STR += 1;
            Player.gold += 1000;
            player.CalculatePlayerStats();
            Player.currentHP = Player.maxHP;
            Player.playerX = 36;
            Player.playerY = 11;
            //player.CharInfo();*/
            //GAMEPLAY LOOP
            while (action != "EXIT")
            {
                //CHOOSE PLAYGROUND
                switch (PlaygroundSwitch.currentPlayground)
                {
                    //MAIN MENU PLAYGROUND
                    case "Menu":
                        switch (action)
                        {
                            case "GAME STARTED":
                                text.PrintText(text.mainMenuText);
                                action = Console.ReadLine();
                                break;

                            case "START":
                                player.CharCreaion();
                                action = Console.ReadLine();
                                break;

                            case "HELP":
                                text.PrintText(text.mainMenuHelp);
                                action = Console.ReadLine();
                                break;

                            default:
                                action = Console.ReadLine();
                                break;

                        }
                        break;
                    //TAVERN PLAYGEOUND
                    case "Tavern":
                        Render2D.path = @"Tavern.txt";
                        render2d.StartRender();
                        while (Player.playerX != 25)
                        {
                            render2d.Render2DMove();
                            if (Player.playerX <= 5)
                            {
                                action = Console.ReadLine();
                                if (action == "SLEEP")
                                {
                                    tavern.TavernSleepMethod();
                                }
                            }
                            else if (Player.playerX == 15 && Player.playerY == 3)
                            {
                                action = Console.ReadLine();
                                if (action == "EAT")
                                {
                                    tavern.TavernEatMethod();
                                }
                                else if (action == "TEST")
                                {
                                   text.ChatMethod(text.testArr);
                                }
                            }
                            else if (Player.playerX > 6 && Player.playerY == 8)
                            {
                                action = Console.ReadLine();
                                if (action == "CHAT")
                                {
                                    tavern.TavernChatMethod();
                                }
                            }
                        }
                        Console.Clear();
                        Player.playerX = 11;
                        Player.playerY = 8;
                        PlaygroundSwitch.currentPlayground = "Town";
                        text.PrintText(text.tavernExitTextArr); 
                        break;

                        //TOWN PLAYGROUND
                    case "Town":
                        Render2D.path = @"Town.txt";
                        render2d.StartRender();
                        while (Player.playerX != 10 && Player.playerX != 17 && Player.playerX != 32 && Player.playerX != 31 && Player.playerX != 22 &&  Player.playerX != 36)
                        {
                            render2d.Render2DMove();
                            if (Player.playerX == 20 && Player.playerY == 8)
                            {
                                Console.WriteLine("Judge: 'Good morning, {0}'", Player.name);
                            }
                        }
                        if (Player.playerX == 10 && Player.playerY == 8)
                        {
                            Console.Clear();
                            PlaygroundSwitch.currentPlayground = "Tavern";
                            text.PrintText(text.townTavernEnterTextArr);
                            Player.playerX = 24;
                            Player.playerY = 5;
                        }   
                        else if (Player.playerX == 17 && Player.playerY == 4)
                        {
                            Console.Clear();
                            PlaygroundSwitch.currentPlayground = "Forge";
                            text.PrintText(text.townForgeEnterTextArr);
                            Player.playerX = 9;
                            Player.playerY = 8;
                        }
                        else if (Player.playerX == 22 && Player.playerY == 9)
                        {
                            Console.Clear();
                            PlaygroundSwitch.currentPlayground = "Temple";
                            text.PrintText(text.townForgeEnterTextArr);
                            Player.playerX = 14;
                            Player.playerY = 2;
                        }
                        else if ((Player.playerX == 31 || Player.playerX == 32) && Player.playerY == 5)
                        {
                            Console.Clear();
                            PlaygroundSwitch.currentPlayground = "Market";
                            text.PrintText(text.townMarketEnterTextArr);
                            Player.playerX = 10;
                            Player.playerY = 10;
                        }
                        else if (Player.playerX == 36 && Player.playerY == 13)
                        {
                            Console.Clear();
                            PlaygroundSwitch.currentPlayground = "Tomb";
                            Player.playerX = 1;
                            Player.playerY = 4;
                        }
                        else
                        {
                            render2d.Render2DMove();
                        }
                           
                            break;

                        //MARKET PLAYGROUND
                    case "Market":
                        Render2D.path = @"Market.txt";
                        render2d.StartRender();
                        while(Player.playerY != 11)
                        {
                            render2d.Render2DMove();
                            if (Player.playerY == 3 || Player.playerY == 6 || Player.playerY == 9)
                            {
                                action = Console.ReadLine();
                                switch (action)
                                {
                                    case "SELL":
                                        market.SellTreasures();
                                        break;

                                    case "GOODS":
                                        market.SeeMarketGoods();
                                        break;

                                    case "SPEC":
                                        market.FindSpecPotions();
                                        break;

                                    case "CHAT":
                                        text.PrintText(text.marketChatArr);
                                        break;
                                }
                            }
                        }
                        Console.Clear();
                        Player.playerX = 31;
                        Player.playerY = 6;
                        PlaygroundSwitch.currentPlayground = "Town";
                        text.PrintText(text.tavernExitTextArr);
                        break;

                        //FORGE Playground
                    case "Forge":
                        Render2D.path = @"Forge.txt";
                        render2d.StartRender();
                        while (Player.playerY != 9)
                        {
                            render2d.Render2DMove();
                            if(Player.playerY == 3)
                            {
                                action = Console.ReadLine();
                                if (action == "GOODS")
                                {
                                    forge.ForgeGoods();
                                }
                                else if (action == "CHAT")
                                {
                                    text.PrintText(text.forgeChatArr);
                                }
                            }
                        }
                        Console.Clear();
                        Player.playerX = 17;
                        Player.playerY = 5;
                        text.PrintText(text.tavernExitTextArr);
                        PlaygroundSwitch.currentPlayground = "Town";
                        break;

                        //TEMPLE Playground
                   case "Temple":
                        Render2D.path = @"Temple.txt";
                        render2d.StartRender();
                        while (Player.playerY != 1)
                        {
                            render2d.Render2DMove();
                            if (Player.playerX == 14 && Player.playerY == 17)
                            {
                                action = Console.ReadLine();
                                if (action == "POTION")
                                {
                                    temple.healingPotionsBuyMethod();
                                }
                                else if (action == "PREACH")
                                {
                                    temple.PreachMethod();
                                }
                                else if (action == "CHAT")
                                {
                                    text.PrintText(text.templeChatArr);
                                }
                            }
                        }
                        Console.Clear();
                        Player.playerX = 22;
                        Player.playerY = 8;
                        text.PrintText(text.tavernExitTextArr);
                        PlaygroundSwitch.currentPlayground = "Town";
                        break;

                    //TOMB Playground
                    case "Tomb":
                        Render2D.path = @"Tomb" + Tomb.currentTombLevel + ".txt";
                        render2d.StartRender();
                        while(Player.playerX != 0)
                        {
                            tomb.randomEncounter();
                            render2d.Render2DMove();
                        }
                        Console.Clear();
                        Player.playerX = 36;
                        Player.playerY = 12;
                        PlaygroundSwitch.currentPlayground = "Town";
                        break;

                    //BATTLE PLAYGROUND
                    case "Battle":
                        battle.battleMethod();
                        break;
                }
                
            }
//-----------END of GAMEPLAY LOOP--------//
        }
    }
}
